<?php

/**
 * Implementation of hook_node_info().
 */
function extranet_feature_company_node_info() {
  $items = array(
    'company' => array(
      'name' => t('Company'),
      'base' => 'node_content',
      'description' => t('A company is an organization or group that we work with or would like to work with. Companies are used to track projects. Some companies have many projects. Companies also have associated contacts.'),
      'has_title' => '1',
      'title_label' => t('Company Name'),
      'help' => '',
    ),
  );
  return $items;
}

<?php

/**
 * Implementation of hook_node_info().
 */
function extranet_feature_lead_node_info() {
  $items = array(
    'lead' => array(
      'name' => t('Lead'),
      'base' => 'node_content',
      'description' => t('A lead is a business lead that is used to let us know the status of sales work. It will be associated with a contact and perhaps a company also.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

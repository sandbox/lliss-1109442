<?php

/**
 * Implementation of hook_node_info().
 */
function extranet_feature_contact_node_info() {
  $items = array(
    'contact' => array(
      'name' => t('Contact'),
      'base' => 'node_content',
      'description' => t('A contact consists of contact information for a client or lead.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}

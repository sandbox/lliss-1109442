<?php

/**
 * Implementation of hook_views_api().
 */
function extranet_feature_sales_lead_views_views_api() {
  return array(
    'api' => '3.0-alpha1',
  );
}

<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function extranet_feature_initial_lead_taxonomy_taxonomy_default_vocabularies() {
  return array(
    'initial_lead_' => array(
      'name' => 'Initial Lead',
      'machine_name' => 'initial_lead_',
      'description' => 'Initial lead source for Lead content type ',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          '0' => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            '0' => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            '0' => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
